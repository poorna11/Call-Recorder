# Call-Recorder

# Recorder Features

1. Automatically Records incoming/outgoing calls which might be single or conference calls.
2. Stores all recordings in internal storage in a folder “AudioRecorder”.
3. Displays Outgoing Call Log which includes phone number,Call Type(Incoming/Outgoing),Call Date,Calling Sim,Call Duration.
4. Displays the email id with current date as signed in by user while login.
5. User has option to call from dial pad or contacts within the application.
6. Recorde automatically stops when call is hung up.
7. All files in "AudioRecord" folder are uploaded to server.

# Compatibility

The application is fully compatible till android 8.1. Some audio glitches can occur in higher android versions due to restrictions on accessing the audio stream of the other end in higher android versions.

# Permissions

Runtime permissions are implemented in code for following permissions

1. READ_CONTACTS
2. RECORD_AUDIO
3. CALL_PHONE
4. PROCESS_OUTGOING_CALLS
5. READ_EXTERNAL_STORAGE
6. WRITE_EXTERNAL_STORAGE
7. READ_PHONE_STATE

# Settings
1. AudioRecorder gives a raw sound stream which id then compressed to wav file. https://developer.android.com/reference/android/media/AudioRecord
2. Sampling rate : 16khz
3. File format : wav
4. Recording channel : Mono
5. Recording source : VOIP (MediaRecorder.AudioSource.VOICE_COMMUNICATION) https://stackoverflow.com/questions/47359587/what-is-the-best-audiosource-setting-for-calls  
6. Noise Suppressor : https://developer.android.com/reference/android/media/audiofx/NoiseSuppressor
7. Automatic Gain Controller: https://developer.android.com/reference/android/media/audiofx/AutomaticGainControl  
8. Acoustic echo canceler: https://developer.android.com/reference/android/media/audiofx/AcousticEchoCanceler

# PhoneState Receiver

The application receives broadcasts on particularly 3 events when phone is :

1. "Ringing"(CALL_STATE_RINGING)
2. "Picked up"(CALL_STATE_OFFHOOK)
3. "Hung up"(CALL_STATE_IDLE)

